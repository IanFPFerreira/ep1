#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "instrumento.hpp"
#include "cliente.hpp"

using namespace std;

string getString()
{
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput()
{
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

string verifica_cpf()
{ 
    string cpf;
    bool p = true;
    do
    {
        cin >> cpf;
        int tam;
        tam = cpf.size();
        if (tam > 11 || tam < 11)
        {
            p = false;
            cin.clear();
            cin.ignore(32767, '\n');
            cout << " Digite novamente o cpf: (11 numeros): ";
        }
        else
        {
            int aux = 0;
            for (int i = 0; i < tam; i++)
            {
                if (((int)cpf[i] > 47) & ((int)cpf[i] < 58))
                {
                    aux++;
                }
            }
            if (aux == 11)
            {
                p = true;
            }
            else
            {
                p = false;
                cout << " Digite novamente o cpf: (11 numeros): ";
            }
        }
    } while (p == false);
    cin.clear();
    cin.ignore(32767, '\n');
    return cpf;
    
}

void Cadastro()
{
    system("clear");

    fstream arquivo;
    string linha;

    string nome;
    string cpf;
    string email;
    char socio;

    cout << " Caro músico ou música...\n";
    cout << " Deseja ser sócio(a)?[S/n]: ";
    socio = getInput<char>();
    if(socio=='S' || socio=='s')
    {
        arquivo.open("doc/socios.txt", ios::out|ios::app);

        cout << " Cadastre-se e entre na melodia da nossa loja:" << endl;
        cout << " Nome: ";
        nome = getString(); 
        cout << " CPF: ";
        verifica_cpf();
        //cpf = getString();
        cout << " Email: ";
        email = getString();


        arquivo << " Nome: " << nome << "\n";
        arquivo << " CPF: " << cpf << "\n";
        arquivo << " email: " << email << "\n";
        arquivo << " __________________________________ \n";
        arquivo.close();
    }else
    {
        arquivo.open("doc/clientes.txt", ios::out|ios::app);

        cout << " Cadastre-se e entre na melodia da nossa loja:" << endl;
        cout << " Nome: ";
        nome = getString(); 
        cout << " CPF: ";
        //cpf = getString();
        verifica_cpf();
        cout << " Email: ";
        email = getString();


        arquivo << " Nome: " << nome << "\n";
        arquivo << " CPF: " << cpf << "\n";
        arquivo << " email: " << email << "\n";
        arquivo << " __________________________________ \n";
        arquivo.close();
    }
    system("clear");

}

int clienteExiste = 0; 
int verificaCadastro()
{
    string nome;
    string cpf;
    char socio;
    string verificadorCPF;

    cout << " Você é sócio(a)?[S/n]: ";
    cin >> socio;

    if(socio=='S' || socio=='s')
    {
        cout << " Informe o CPF para que possamos confirmar o cadastro:" << endl;
        cin >> verificadorCPF;

        ifstream arquivoL;

        string linha = " ";
        arquivoL.open("doc/socios.txt");

        bool procurandoCliente = false;
        while (linha != "")
        {
            getline(arquivoL, linha);
            istringstream iss(linha);

            iss >> nome; 
            //getchar();
            iss >> cpf;

            if(cpf == verificadorCPF)
            {
                procurandoCliente = true;
                clienteExiste = 1;
                break;
            }
        }
        if(procurandoCliente == false)
        {
            getchar(); 
            Cadastro();
        }

    }
    else
    {
        cout << " Informe o CPF para que possamos confirmar o cadastro:" << endl;
        cin >> verificadorCPF;

        ifstream arquivoL;

        string linha = " ";
        arquivoL.open("doc/clientes.txt");

        bool procurandoCliente = false;
        while (linha != "")
        {
            getline(arquivoL, linha);
            istringstream iss(linha);

            iss >> nome; 
            //getchar();
            iss >> cpf;

            if(cpf == verificadorCPF)
            {
                procurandoCliente = true;
                clienteExiste = 1;
                break;
            }
        }
        if(procurandoCliente == false)
        {
            getchar(); 
            Cadastro();
        }
    }

    return clienteExiste;
}

void Estoque()
{
    system("clear");

    fstream arquivo;
    string linha;

    int ID;
    string tipo;
    string marca;
    string categoria;
    double valor;
    int qtd;

    string senha; // senha = "som"

    cout << " Digite a senha para o cadastro do novo instrumento: ";
    senha = getString();
    if(senha == "som")
    {
        cout << " Cadastro de um novo instrumento" << endl;
        cout << " ID: ";
        ID = getInput<int>();
        cout << " Tipo: ";
        tipo = getString();
        cout << " Marca: ";
        marca = getString();
        cout << " Categoria: ";
        categoria = getString();
        cout << " Valor: ";
        valor = getInput<double>();
        cout << " Quantidade: ";
        qtd = getInput<int>();
        
        if(tipo=="guitarra"||tipo=="Guitarra")
        {
            arquivo.open("doc/instrumentos/guitarra.txt", ios::out|ios::app);
            arquivo << " ID: " << ID << "\n";
            arquivo << " Tipo: " << tipo << "\n";
            arquivo << " Marca: " << marca << "\n";
            arquivo << " Categoria: " << categoria << "\n";
            arquivo << " Valor: R$" << valor << "\n";
            arquivo << " Quantidade: " << qtd << "\n";
            arquivo << " __________________________________ \n";
            arquivo.close();
        }
        else if(tipo=="violino"||tipo=="Violino")
        {
            arquivo.open("doc/instrumentos/violino.txt", ios::out|ios::app);
            arquivo << " ID: " << ID << "\n";
            arquivo << " Tipo: " << tipo << "\n";
            arquivo << " Marca: " << marca << "\n";
            arquivo << " Categoria: " << categoria << "\n";
            arquivo << " Valor: R$" << valor << "\n";
            arquivo << " Quantidade: " << qtd << "\n";
            arquivo << " __________________________________ \n";
            arquivo.close();
        }
        else if(tipo=="baixo"||tipo=="Baixo")
        {
            arquivo.open("doc/instrumentos/baixo.txt", ios::out|ios::app);
            arquivo << " ID: " << ID << "\n";
            arquivo << " Tipo: " << tipo << "\n";
            arquivo << " Marca: " << marca << "\n";
            arquivo << " Categoria: " << categoria << "\n";
            arquivo << " Valor: R$" << valor << "\n";
            arquivo << " Quantidade: " << qtd << "\n";
            arquivo << " __________________________________ \n";
            arquivo.close();
        }
        else if(tipo=="violão"||tipo=="Violão"||tipo=="violao"||tipo=="Violao")
        {
            arquivo.open("doc/instrumentos/violao.txt", ios::out|ios::app);
            arquivo << " ID: " << ID << "\n";
            arquivo << " Tipo: " << tipo << "\n";
            arquivo << " Marca: " << marca << "\n";
            arquivo << " Categoria: " << categoria << "\n";
            arquivo << " Valor: R$" << valor << "\n";
            arquivo << " Quantidade: " << qtd << "\n";
            arquivo << " __________________________________ \n";
            arquivo.close();
        }
        else if(tipo=="bateria"||tipo=="bateria")
        {
            arquivo.open("doc/instrumentos/bateria.txt", ios::out|ios::app);
            arquivo << " ID: " << ID << "\n";
            arquivo << " Tipo: " << tipo << "\n";
            arquivo << " Marca: " << marca << "\n";
            arquivo << " Categoria: " << categoria << "\n";
            arquivo << " Valor: R$" << valor << "\n";
            arquivo << " Quantidade: " << qtd << "\n";
            arquivo << " __________________________________ \n";
            arquivo.close();
        }


    }
    else
    {
        cout << " ERRO... SENHA INVÁLIDA! Aperte qualquer tecla e enter para retornar a tela inicial..." << endl;
        string tecla;
        cin >> tecla;
    }
    system("clear");
}

void vendaInstrumentos()
{
    system("clear");

    ifstream arquivoL;
    string linha;
    int escolherID;
    char escolhaCadastro;

    cout << " Caro músico ou música, você ja possui cadastro?[S/n]:";
    cin >> escolhaCadastro;

    if(escolhaCadastro=='S' || escolhaCadastro=='s')
    {
        if(verificaCadastro() != 1)
        {
           return;
           system("clear");
        } 
    }else if(escolhaCadastro=='N' || escolhaCadastro=='n')
    {
        getchar();
        Cadastro();
        system("clear");
    }

    cout << " Bem-vindo. Escolha uma das melodias!" << endl;
    cout << " ___________________________________ " << endl;
    cout << "|(1) Baixo                          |" << endl;
    cout << "|(2) Bateria                        |" << endl;
    cout << "|(3) Guitarra                       |" << endl;
    cout << "|(4) Violão                         |" << endl;
    cout << "|(5) Violino                        |" << endl;
    cout << " ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´ " << endl;
    int escolha;
    cin >> escolha;

    switch(escolha)
    {   case 1:
            arquivoL.open("doc/instrumentos/baixo.txt", ios::in);

            cout << " Veja nossa partitura instrumental!" << endl;
            cout << " ___________________________________ " << endl;
            if(arquivoL.is_open())
            {
                while(getline(arquivoL,linha))
                {
                    cout << linha << endl;
                }
            }
            break;
        case 2:
            arquivoL.open("doc/instrumentos/bateria.txt", ios::in);

            cout << " Veja nossa partitura instrumental!" << endl;
            cout << " ___________________________________ " << endl;
            if(arquivoL.is_open())
            {
                while(getline(arquivoL,linha))
                {
                    cout << linha << endl;
                }
            }
            break;
        case 3:
            arquivoL.open("doc/instrumentos/guitarra.txt", ios::in);

            cout << " Veja nossa partitura instrumental!" << endl;
            cout << " ___________________________________ " << endl;
            if(arquivoL.is_open())
            {
                while(getline(arquivoL,linha))
                {
                    cout << linha << endl;
                }
            }
            break;
        case 4:
            arquivoL.open("doc/instrumentos/vilao.txt", ios::in);

            cout << " Veja nossa partitura instrumental!" << endl;
            cout << " ___________________________________ " << endl;
            if(arquivoL.is_open())
            {
                while(getline(arquivoL,linha))
                {
                    cout << linha << endl;
                }
            }
            break;
        case 5:
            arquivoL.open("doc/instrumentos/violino.txt", ios::in);

            cout << " Veja nossa partitura instrumental!" << endl;
            cout << " ___________________________________ " << endl;
            if(arquivoL.is_open())
            {
                while(getline(arquivoL,linha))
                {
                    cout << linha << endl;
                }
            }
            break;

    }
    cout << " Digite o ID do instrumento para fazer a compra: ";
    cin >> escolherID;
    

    system("clear");

}

void modoRecomenda()
{   
    system("clear");
    ifstream arquivoL;
    ifstream arquivoG;
    ifstream arquivoB;
    ifstream arquivoV;
    ifstream arquivoA;
    string linha;

    string tecla;
    string cpf;
    verificaCadastro();
    system("clear");
    cout << " Escreva qual instrumento você já possui: ";
    string instrumento;
    cin >> instrumento;

    if(instrumento=="guitarra"||instrumento=="Guitarra")
    {
        arquivoB.open("doc/instrumentos/bateria.txt", ios::in);

        cout << " Veja nossa partitura instrumental!" << endl;
        cout << " ___________________________________ " << endl;
        if(arquivoB.is_open())
        {
            while(getline(arquivoB,linha))
            {
                cout << linha << endl;
            }
        }
        arquivoL.open("doc/instrumentos/baixo.txt", ios::in);

        if(arquivoL.is_open())
        {
            while(getline(arquivoL,linha))
            {
                cout << linha << endl;
            }
        }
        cout <<"Recomendamos que você compre uma bateria e um baixo...\n";
        cout <<"E COMECE JÁ A SUA BANDA!!" << endl;
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }
    else if(instrumento=="bateria"||instrumento=="Bateria")
    {
        arquivoL.open("doc/instrumentos/baixo.txt", ios::in);

        cout << " Veja nossa partitura instrumental!" << endl;
        cout << " ___________________________________ " << endl;
        if(arquivoL.is_open())
        {
            while(getline(arquivoL,linha))
            {
                cout << linha << endl;
            }
        }
        arquivoG.open("doc/instrumentos/guitarra.txt", ios::in);

        if(arquivoG.is_open())
        {
            while(getline(arquivoG,linha))
            {
                cout << linha << endl;
            }
        }
        cout <<"Recomendamos que você compre uma guitarra e um baixo...\n";
        cout <<"E COMECE JÁ A SUA BANDA!!" << endl;
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }
    else if(instrumento=="violino"||instrumento=="Violino")
    {
        arquivoA.open("doc/instrumentos/violao.txt", ios::in);

        cout << " Veja nossa partitura instrumental!" << endl;
        cout << " ___________________________________ " << endl;
        if(arquivoA.is_open())
        {
            while(getline(arquivoA,linha))
            {
                cout << linha << endl;
            }
        }
        arquivoG.open("doc/instrumentos/guitarra.txt", ios::in);

        if(arquivoG.is_open())
        {
            while(getline(arquivoG,linha))
            {
                cout << linha << endl;
            }
        }
        cout <<"Recomendamos que você compre uma guitarra ou um violão, pois são instrumentos de cordas também, mas...\n";
        cout <<"O VIOLINO É O MELHOR INSTRUMENTO!!" << endl;
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }
    else if(instrumento=="violao"||instrumento=="violão"||instrumento=="Violao"||instrumento=="Violão")
    {
        arquivoG.open("doc/instrumentos/guitarra.txt", ios::in);

        cout << " Veja nossa partitura instrumental!" << endl;
        cout << " ___________________________________ " << endl;
        if(arquivoG.is_open())
        {
            while(getline(arquivoG,linha))
            {
                cout << linha << endl;
            }
        }
        cout <<"Recomendamos que você compre uma guitarra...\n";
        cout <<"SABENDO VIOLÃO, JÁ É UMA PARTE DO CAMINHO PARA SE TORNAR UMA ESTRELA DO ROCK!!" << endl;
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }
    else if(instrumento=="baixo"||instrumento=="Baixo")
    {
        arquivoB.open("doc/instrumentos/bateria.txt", ios::in);

        cout << " Veja nossa partitura instrumental!" << endl;
        cout << " ___________________________________ " << endl;
        if(arquivoB.is_open())
        {
            while(getline(arquivoB,linha))
            {
                cout << linha << endl;
            }
        }
        arquivoG.open("doc/instrumentos/guitarra.txt", ios::in);

        if(arquivoG.is_open())
        {
            while(getline(arquivoG,linha))
            {
                cout << linha << endl;
            }
        }
        cout <<"Recomendamos que você compre uma bateria e uma guitarra...\n";
        cout <<"E COMECE JÁ A SUA BANDA!!" << endl;
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }
    else
    {
        cout <<"Não há uma recomendação para você!!";
        cout <<"Aperte qualquer tecla e enter, para voltar a tela inicial!";
        cin >> tecla;
        system("clear");
    }

}


int main(){
    
    int comando = -1;

    cout << " __________________________________"  << endl;
    cout << "|BEM-VINDO A DREAM INSTRUMENTS!    |" << endl;
    cout << "|´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´|" << endl;

    while(comando != 0){
        cout << "|Escolha uma da opções!            |" << endl;
        cout << "|__________________________________|" << endl;
        cout << "|(1) Cadastre-se como Cliente      |" << endl;
        cout << "|(2) Cadastre um novo instrumento  |" << endl;
        cout << "|(3) Compre seu instrumento aqui   |" << endl;
        cout << "|(4) Não tenho dúvidas no que tocar|" << endl;
        cout << "|(0) Sair                          |" << endl;
        cout << " ´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´"  << endl;
        comando = getInput<int>();
        
        switch(comando){
            case 1:
                Cadastro();
                break;
            case 2:
                Estoque();
                break;
            case 3:
                vendaInstrumentos();
                break;
            case 4:
                cout << " Recomendações da loja só para você!" << endl;
                cout << "__________________________________" << endl;
                modoRecomenda();
                break;
            case 0:
                break;                
            default:
                cout << " Opção inválida!" << endl;

        } 

    }

    return 0;
}
