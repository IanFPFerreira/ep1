#include "instrumento.hpp"
#include <iostream>
#include <string>
using namespace std;

Instrumento::Instrumento(int ID, string tipo, string marca, string categoria, double valor, int qtd){
    set_ID(ID);
    set_tipo(tipo);
    set_marca(marca);
    set_categoria(categoria);
    set_valor(valor);   
    set_qtd(qtd);
}
Instrumento::~Instrumento(){}

void Instrumento::set_ID(int ID){
    this->ID = ID;
}
int Instrumento::get_ID(){
    return ID;
}

void Instrumento::set_tipo(string tipo){
    this->tipo = tipo;
}
string Instrumento::get_tipo(){
    return tipo;
}

void Instrumento::set_marca(string marca){
    this->marca = marca;
}
string Instrumento::get_marca(){
    return marca;
}

void Instrumento::set_categoria(string categoria){
    this->categoria = categoria;
}
string Instrumento::get_categoria(){
    return categoria;
}

void Instrumento::set_valor(double valor){
    this->valor = valor;
}
double Instrumento::get_valor(){
    return valor;
}

void Instrumento::set_qtd(int qtd){
    this->qtd = qtd;
}
int Instrumento::get_qtd(){
    return qtd;
}

void Instrumento::imprime_dados(){
    cout << "ID: " << get_ID() << endl;
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Marca: " << get_marca() << endl;    
    cout << "Categoria: " << get_categoria() << endl;
    cout << "Valor: R$" << get_valor() << endl;
    cout << "Quantidade: " << get_qtd() << endl;
}