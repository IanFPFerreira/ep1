#include "cliente.hpp"
#include <iostream>
#include <string>
using namespace std;

Cliente::Cliente(string nome, string cpf, string email, char socio){
    set_nome(nome);
    set_cpf(cpf);
    set_email(email);
    set_socio(socio);     
}

Cliente::~Cliente(){}

void Cliente::set_nome(string nome){
    this->nome = nome;
}
string Cliente::get_nome(){
    return nome;
}

void Cliente::set_cpf(string cpf){
    this->cpf = cpf;
}
string Cliente::get_cpf(){
    return cpf;
}

void Cliente::set_email(string email){
    this->email = email;
}
string Cliente::get_email(){
    return email;
}

void Cliente::set_socio(char socio){
    this->socio = socio;
}
char Cliente::get_socio(){
    return socio;
}

void Cliente::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;  
    cout << "Email: " << get_email() << endl;
    cout << "Sócio: ";
    if(get_socio() == 's' || get_socio() == 'S')
        cout << "Sim" << endl;
    else
        cout << "Não" << endl;
} 