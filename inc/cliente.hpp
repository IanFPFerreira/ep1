#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente{
private:
    string nome;
    string cpf;
    string email;
    char socio;
public:
    Cliente();
    Cliente(string nome, string cpf, string email, char socio);
    ~Cliente(); 
    
    string get_nome();
    void set_nome(string nome);
    string get_cpf();
    void set_cpf(string cpf);
    string get_email();
    void set_email(string email);
    char get_socio();
    void set_socio(char socio);

    virtual void imprime_dados();
    


};

#endif