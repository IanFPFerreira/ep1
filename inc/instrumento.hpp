#ifndef INSTRUMENTO_HPP
#define INSTRUMENTO_HPP

#include <string>
using namespace std;

class Instrumento{
    private:
        int ID;
        string tipo;
        string marca;
        string categoria;
        double valor;
        int qtd;
        
    public:
        Instrumento();
        Instrumento(int ID, string tipo, string marca, string categoria, double valor, int qtd);
        ~Instrumento();

        void set_ID(int ID);
        int get_ID();
        void set_tipo(string tipo);
        string get_tipo();
        void set_marca(string marca);
        string get_marca();
        void set_categoria(string categoria);
        string get_categoria();
        void set_valor(double valor);
        double get_valor();
        void set_qtd(int qtd);
        int get_qtd();

        virtual void imprime_dados();
};


#endif