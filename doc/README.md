# BEM-VINDO A DREAM INSTRUMENTS! 

    Ian Fillipe Pontes Ferreira
    18/0102087
    Turma B

## Compilar e executar

- Digite *make* no terminal, no diretório do ep, para compilar

- Digite *make run* para executar o programa;

O menu consiste nas abas *Cadastro*, *Estoque*, *Compra* e *Recomendação*.
# Menus:

## Cadastro:
    Onde será feito o cadastro do cliente, perguntando se o mesmos quer ser sócio, ou apenas um cliente convencional.
    
## Estoque:
    Onde será feito o cadastro dos produtos, nos caso, dos novos instrumentos.
### Para acessar a aba *Estoque* é necessário digitar a senha:
    som

## Compra:
    Primeiramente será feito uma verificação do CPF, para saber se o cliente já é cadastrado, em caso negativo ou com a digitação de um CPF inválido, o usúario será 
    direcionado para o menu de cadastro antes de ser feita a compra.
    
## Recomendação:
    Com a entrada do usuário do que ele já comprou ou possui, diferentes recomendações serão mostradas ao mesmo. 

## Bibliotecas utilizadas:
    <iostream>
    <fstream>
    <sstream>
    <string>
    <vector>